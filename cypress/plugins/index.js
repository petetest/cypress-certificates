// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const { exec } = require('child_process');

const doExec = (cmd) => {
  exec(cmd, (err, stdout, stderr) => {
    if (err) {
      // node couldn't execute the command
      return;
    }
 
    // the *entire* stdout and stderr (buffered)
    console.log(`Doing cmd: ${cmd}`)
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
}
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config

    on('task', {
      loadCert (name) {
        console.log(name)
       
        doExec(`pk12util -d sql:$HOME/.pki/nssdb -i /home/user/cypress-certificates/${name}.p12 -W ""`)

        doExec('certutil -L -d sql:$HOME/.pki/nssdb')
        return null
      },

      removeCert (name) {
        console.log(name)
        
        doExec(`certutil -D -d sql:/home/user/.pki/nssdb -n "${name} - Client Certificate Demo"`)
        doExec('certutil -L -d sql:$HOME/.pki/nssdb')
        return null
      }
    })
  }
