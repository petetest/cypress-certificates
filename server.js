const express = require('express')
const fs = require('fs')
const https = require('https')
const cors = require('cors')


const opts = { key: fs.readFileSync('server_key.pem')
             , cert: fs.readFileSync('server_cert.pem')
	, requestCert: true
, rejectUnauthorized: false
, ca: [ fs.readFileSync('server_cert.pem') ]
             }

const app = express()

app.use(cors())

app.get('/', (req, res) => {
const cert = req.connection.getPeerCertificate()
console.log(req.client.authorized)
	if (req.client.authorized) {
		res.send(`<title>Hello ${cert.subject.CN}, your certificate was issued by ${cert.issuer.CN}!</title>`)
	} else if (cert.subject) {
		res.status(403)
		   .send(`<title>Sorry ${cert.subject.CN}, certificates from ${cert.issuer.CN} are not 	welcome here.</title>`)
	} else {
		res.status(401)
		   .send(`<title>Sorry, but you need to provide a client certificate to continue.</title>`)
	}
	//res.send('<a href="authenticate">Log in using client certificate</a>')
})

app.get('/authenticate', (req, res) => {
	const cert = req.connection.getPeerCertificate()
console.log(req.client.authorized)
	if (req.client.authorized) {
		res.send(`Hello ${cert.subject.CN}, your certificate was issued by ${cert.issuer.CN}!`)
	} else if (cert.subject) {
		res.status(403)
		   .send(`Sorry ${cert.subject.CN}, certificates from ${cert.issuer.CN} are not 	welcome here.`)
	} else {
		res.status(401)
		   .send(`Sorry, but you need to provide a client certificate to continue.`)
	}
})

https.createServer(opts, app).listen(9999)
