## Getting started

This is a simple web app to simulate the issues we have with certificates on our sites when using cypress

Although we would normally require a certificate for the initial call I have ignored this for the purpose of the real issue

I have modified cypress to process certificates using the cy.visit and cy.request but cypress still expects certificates in the browser for any further calls.
The repository is to simulate the need for certifates on that second call.

Run:

node server.js

node testme.js

Then within cypress navigate to "https://localhost:9998"

When you then click the link presented the website will make a second call to "https://localhost:9999" and will respond with your certificate information if found. Cypress does not control this second call in terms of the certificate presented, it expects that it is in the browser.

Hopefully we can get cypress to load that certificate before making the call.