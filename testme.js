const express = require('express')
const fs = require('fs')
const https = require('https')

//const opts = {}; 
const opts = { key: fs.readFileSync('server_key.pem')
             , cert: fs.readFileSync('server_cert.pem')
//	, requestCert: true
//, rejectUnauthorized: false
, ca: [ fs.readFileSync('server_cert.pem') ]
            }
const html = fs.readFileSync('hello.html');

const app = express()

app.get('/', (req, res) => {
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.write(html);  
        res.end();
	
})



https.createServer(opts, app).listen(9998)
